﻿<volume level="100"/><voice required="Name=IVONA 2 Emma OEM"><rate absspeed="-4">20.<silence msec="500"/></rate></voice>
<volume level="100"/><voice required="Name=IVONA 2 Brian OEM"><rate absspeed="-4">Pound an almond, and the clear white colour will be altered into a dirty one, and the sweet taste into an oily one.<silence msec="500"/></rate></voice>
<volume level="100"/><voice required="Name=IVONA 2 Brian OEM"><rate absspeed="-4">What real alteration can the beating of the pestle make in any body, but an alteration of the texture of it?<silence msec="500"/></rate></voice>
<volume level="100"/><voice required="Name=IVONA 2 Brian OEM"><rate absspeed="-4"><silence msec="500"/></rate></voice>
