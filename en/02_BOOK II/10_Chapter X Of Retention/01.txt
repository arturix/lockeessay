﻿<volume level="100"/><voice required="Name=IVONA 2 Emma OEM"><rate absspeed="-4">1. Contemplation.<silence msec="500"/></rate></voice>
<volume level="100"/><voice required="Name=IVONA 2 Brian OEM"><rate absspeed="-4">The next faculty of the mind, whereby it makes a further progress towards knowledge, is that which I call retention; or the keeping of those simple ideas which from sensation or reflection it hath received.<silence msec="500"/></rate></voice>
<volume level="100"/><voice required="Name=IVONA 2 Brian OEM"><rate absspeed="-4">This is done two ways.<silence msec="500"/></rate></voice>
<volume level="100"/><voice required="Name=IVONA 2 Brian OEM"><rate absspeed="-4">First, by keeping the idea which is brought into it, for some time actually in view, which is called contemplation.<silence msec="500"/></rate></voice>
<volume level="100"/><voice required="Name=IVONA 2 Brian OEM"><rate absspeed="-4"><silence msec="500"/></rate></voice>
