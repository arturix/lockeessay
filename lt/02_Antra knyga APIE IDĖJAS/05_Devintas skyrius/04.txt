4. Kaip da�nai pats �mogus gali pasteb�ti, kad jo
intelektas, giliai pasin�r�s � tam tikr� objekt� kontempliavim�
ir kruop��iai tiriantis �ia gl�din�ias id�jas,
nepastebi skamban�i� k�n� poveikio klausos organui,
nors toks pat j� sukeltas pokytis paprastai sukurdavo
garso id�j�. Organas gali patirti pakankam� post�m�,
ta�iau jeigu intelektas neatkreips � j� d�mesio, nebus
jokio suvokimo; ir nors ausyje vyks jud�jimas, kuris
paprastai sukurdavo garso id�j�, joks garsas nebus girdimas.
Poj��io nebuvim� �iuo atveju lemia ne koks
nors organo tr�kumas ar tai, kad �mogaus ausys yra
veikiamos silpniau negu kitais kartais, kai jis gird�davo,
- nors tai, kas paprastai sukeldavo id�j�, ir patenka
per �prastin� organ�, ta�iau intelekto nepastebima,
ne�spaud�ia sieloje jokios id�jos, tod�l n�ra jokio suvokimo.
Taigi ten, kur yra jutimas arba suvokimas, i� tikr�j�
sukeliama tam tikra id�ja ir ji yra intelekte.