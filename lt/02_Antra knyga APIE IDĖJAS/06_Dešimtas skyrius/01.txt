1. Kitas intelekto sugeb�jimas, kurio d�ka jis �engia
tolesn� �ingsn� pa�inimo link, yra tas, kur� a� vadinu
i�saugojimu, arba i� poj��io ar refleksijos gaut� paprast�
id�j� i�laikymu. Tai vyksta dviem b�dais. Pirmasis
b�das, kai � intelekt� patekusi id�ja kur� laik� i�laikoma
jo akiratyje, vadinamas kontempliacija.